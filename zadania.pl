/**
 * jest_krewnym(+X:term, +Y:term) is semidet
 * jest_krewnym(+X:term, -Y:term) is nondet
 * jest_krewnym(-X:term, +Y:term) is nondet
 * jest_krewnym(-X:term, -Y:term) is nondet
 * 
 * "jest_krewnym" jest prawdziwe dla takich X i Y,
 * gdzie X i Y są różne i współdzielą przodka.
 *
 * @arg X imię pierwszej osoby
 * @arg Y imię drugiej osoby
 * 
 * @tbd Należy zastąpić fail czymś rozsądnym.
 *      Być może trzeba będzie dodać drugą klauzulę.
 *      Można założyć, że predykat 'jest_przodkiem/2' jest już zdefiniowany (na dole pliku).
 */
jest_krewnym(X, Y) :- fail.

/**
 * jest_potomkiem(+X:term, +Y:term) is semidet
 * jest_potomkiem(+X:term, -Y:term) is nondet
 * jest_potomkiem(-X:term, +Y:term) is nondet
 * jest_potomkiem(-X:term, -Y:term) is nondet
 * 
 * "jest_potomkiem" jest prawdziwe dla takich X i Y,
 * gdzie X jest potomkiem Y.
 *
 * @arg X imię pierwszej osoby
 * @arg Y imię drugiej osoby
 * 
 * @tbd Należy zastąpić fail czymś rozsądnym.
 *      Być może trzeba będzie dodać drugą klauzulę.
 *      Można założyć, że predykat 'jest_przodkiem/2' jest już zdefiniowany (na dole pliku).
 */
jest_potomkiem(X, Y) :- fail.

/**
 * jest_szwagrem(+X:term, +Y:term) is semidet
 * jest_szwagrem(+X:term, -Y:term) is nondet
 * jest_szwagrem(-X:term, +Y:term) is nondet
 * jest_szwagrem(-X:term, -Y:term) is nondet
 * 
 * "jest_szwagrem" jest prawdziwe dla takich X i Y,
 * gdzie X jest szwagrem Y. 
 * Bycie szwagrem jest zdefiniowane jako bycie bratem żony lub (symetrycznie) mężem siostry.
 *
 * @arg X imię pierwszej osoby
 * @arg Y imię drugiej osoby
 * 
 * @tbd Należy zastąpić fail czymś rozsądnym.
 *      Być może trzeba będzie dodać drugą klauzulę.
 *      Można założyć, że mamy do dyspozycji predykaty jest_bratem/2 i jest_żoną/2, jest_siostrą/2, jest_mężem/2.
 *      Bonus: pełna pula punktów będzie przyznana jeżeli wykorzystane zostaną tylko dwa z powyższych predykatów (np. jest_bratem/2 i jest_żoną/2).
 */
jest_szwagrem(X, Y) :- fail.


/**
 * jest_przodkiem(+X:term, +Y:term) is semidet
 * jest_przodkiem(+X:term, -Y:term) is nondet
 * jest_przodkiem(-X:term, +Y:term) is nondet
 * jest_przodkiem(-X:term, -Y:term) is nondet
 * 
 * "jest_przodkiem" jest prawdziwe dla takich X i Y,
 * gdzie X jest przodkiem Y.
 *
 * @arg X imię przodka
 * @arg Y imię potomka
 * 
 */
jest_przodkiem(X, Y) :- jest_rodzicem(X, Y).
jest_przodkiem(X, Y) :- jest_rodzicem(X, Z), 
                        jest_przodkiem(Z, Y).

/**
 * Fakty, z których można korzystać w regułach:
 * 
 * jest_rodzicem(X,Y) - X jest rodzicem Y
 * jest_bratem(X,Y) - X jest bratem Y
 * jest_siostrą(X,Y) - X jest siostrą Y
 * jest_mężem(X,Y) - X jest mężem Y
 * jest_żoną(X,Y) - X jest żoną Y
 */
:- dynamic jest_rodzicem/2.
:- dynamic jest_bratem/2.
:- dynamic jest_siostrą/2.
:- dynamic jest_mężem/2.
:- dynamic jest_żoną/2.
