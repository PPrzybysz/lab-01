# Lab 01. Zapytania i reguły

Celem tego laboratorium jest zapoznanie się z podstawowymi elementami języka Prolog: zapytaniami i regułami.

## Notatniki

- [zapytania](https://swish.swi-prolog.org?code=https://gitlab.com/agh-courses/2023-2024/loginf/lab-01/-/raw/master/notebooks/01_zapytania.swinb)

- [reguły](https://swish.swi-prolog.org?code=https://gitlab.com/agh-courses/2023-2024/loginf/lab-01/-/raw/master/notebooks/02_reguly.swinb)

## Zadania

Uzupełnij brakujące reguły w pliku `zadania.pl` zgodnie z komentarzami `@tbd` w dokumentacji kodu (dokumentacja jest zapisana zgodnie z [regułami sztuki](https://www.swi-prolog.org/pldoc/doc_for?object=section(%27packages/pldoc.html%27))). 

Ten plik też można otworzyć w [środowisku swish](https://swish.swi-prolog.org?code=https://gitlab.com/agh-courses/2023-2024/loginf/lab-01/-/raw/master/zadania.pl).